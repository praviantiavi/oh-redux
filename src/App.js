// import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux'
import store from './redux/store';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';


function App() {
  return (
   <Provider store={store}>
    <div className="App">
      <Router>
        <Switch>          
          <Route path="/" exact component={Login}/>
          <Route path="/home" component={Home}/>
          <Route path="/register" component={Register}/>
        </Switch>
      </Router>
    </div>
   </Provider>  
  );
}

export default App;
