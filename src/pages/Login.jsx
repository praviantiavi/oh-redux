import { Link, useHistory } from "react-router-dom"
import { useState } from 'react'
import axios from "axios";
import { useDispatch } from "react-redux";


const Login = () => {
   const [username, setUsername] = useState();
   const [password, setPassword] = useState(); 
   const dispatch = useDispatch();
   const history = useHistory();
    // console.log(history);

   const checkLogin = async () => {
       try {
            const dataInput = {
                username,
                password,
            };
           const response =  await axios.post(
            `${process.env.REACT_APP_BASE_API_URL}/auth/login`,
            dataInput
            );
            console.log(response);
            localStorage.setItem("token", response.data.token);
            dispatch({type:"IS_LOGIN", payload: true})
            history.push("/home")
       } catch(err){
           console.log(err)
       }
       
   }
    return (
        <>
            <h1>Halaman Home</h1>
            
            <input type="text" placeholder="username" onChange={(e) => setUsername(e.target.value)} />
            <br />
            <br />
            <input type="password" placeholder="password" onChange={(e) => setPassword(e.target.value)} />
            <br />
            <br />
            <button onClick={() => checkLogin()}>Login</button>
            <br />
            <Link to="/register">Go to Register</Link>
        </>
    )
}


export default Login