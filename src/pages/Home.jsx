import { useEffect } from "react";
import axios from "axios";

const Home = () => {

    useEffect(() => {
       getUser()
    }, [])
    const getUser = async () => {
        try {
            
            const response =  await axios.get(
             `http://localhost:3001/users`,
             
             );
             console.log(response);
             
            
             
        } catch(err){
            console.log(err)
        }
        
    }
    
    return (
        <h1>Halaman Home</h1>
    )
}

export default Home