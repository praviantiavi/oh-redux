import { useState } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom';




const Register = () => {
    const [name, setNama] = useState();
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();

    const addData = async () => {
        try {
           const dataInput = {
               name,
               username,
               password,
           }; 
            const response = await axios.post(
                `${process.env.REACT_APP_BASE_API_URL}/auth/register`,
                dataInput
             );
             console.log(response);

        } catch (err) {
            console.log(err);
        }
    }

    return (
       <> 
        <h1>Halaman Register</h1>
        <input 
            type="text" 
            placeholder="name" 
            onChange={(e) => setNama (e.target.value) } 
        />
        <br />
        <br />
        <input 
            type="text" 
            placeholder="username" 
            onChange={(e) => setUsername (e.target.value) }
        />
        <br />
        <br />
        <input 
            type="password" 
            placeholder="password"
            onChange={(e) => setPassword (e.target.value) } //biar ga error password harus lebih dr 5(?)   
        />
        <br />
        <br />
        <button onClick={() => addData()} >Submit</button>
        <br />
        <Link to="/">Go to Login</Link>
       </> 
    )
}

export default Register