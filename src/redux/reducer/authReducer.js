const initialState = {
    name: "",
    username: "",
    isLogin: false
};

const authReducer = (state = initialState, action) => {
    const { type, payload } = action;

    switch(type) {
        case "GET_NAME": {
            return {
                ...state,
                name: payload   
            } 
        }
        case "IS_LOGIN": {
            return {
                ...state,
                isLogin: payload
            }
        }
        default: {
            return {
                ...state
            }
        }
    }
}

export default authReducer;